
import java.util.Scanner;

public class MagicNumber {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter the number to be check  ...");

		int n, sqn1, sqn2, revnum = 0;
		n = sc.nextInt();

		sqn1 = n * n;
		System.out.println("print sq of given number" + sqn1);// 144

		revnum = getReverseNumber(sqn1);

		System.out.println("reverce for the given number is" + revnum);// 441

		sqn2 = (int) Math.sqrt(revnum);// 21

		System.out.println("print sq root for reverse " + sqn2);// 21
		revnum = getReverseNumber(sqn2);

		System.out.println("print the reverse number " + revnum);// 12

		if (n == revnum) {
			System.out.println("The given number is Magic number=" + revnum);
		} else
			System.out.println("The given number is not Magic number=" + revnum);
	}

	private static int getReverseNumber(int n) {
		int sum = 0, rem;
		while (n > 0) {
			rem = n % 10; // where we do module app we get some reminder
			sum = sum * 10 + rem; // it will give multipule values
			n = n / 10; // here will check the n value for while loop
		}
		return sum;
	}

}
