import java.util.Scanner;

public class PalindromEx {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter the number to be check  ...");

		int n, r, sum = 0;
		n = sc.nextInt();

		int temp = n;// saving given number in T

		while (n > 0) {
			r = n % 10; // where we do module app we get some reminder
			sum = sum * 10 + r; // it will give reverse value//1
			n = n / 10; // here will check the n value for while loop
		}

		if (temp == sum) {
			System.out.println("the given number is Palindrom" + temp + "=" + sum);
		} else {
			System.out.println("the given number is a not Palindrom " + temp + "=" + sum);
		}
	}
}
