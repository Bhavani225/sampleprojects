import java.util.Scanner;

public class AddtheNumber {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter the number to be added ...");
		int n = sc.nextInt();
		int R, Sum = 0;
		

		while (n > 0) {

			R = n % 10; // where we do module app we get some reminder
			Sum = Sum + R; // init sum will be zero and d will be reminder
			n = n / 10; // here will check the n value for while loop678
		}

		System.out.println("Add of all the digits in the given number " + Sum);

	}

}
